### Maven
```xml
<repository>
  <id>yaml-util</id>
  <url>https://gitlab.com/api/v4/projects/14669482/packages/maven</url>
</repository>

<dependency>
  <groupId>com.andrei1058.yamlutil</groupId>
  <artifactId>yaml-util</artifactId>
  <version>VERSION_HERE</version>
</dependency>

```